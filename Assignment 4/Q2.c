#include <stdio.h>
int main ()
{
	printf("This will calculate the volume of a cone\n");
	float h,r;
	printf("Enter the height of the cone: ");
	scanf("%f",&h);
	printf("Enter the radius of the base: ");
	scanf("%f",&r);
	float v=3.14*r*r*h/3;
	printf("The volume of the cone = %f",v);
	return 0;
}
